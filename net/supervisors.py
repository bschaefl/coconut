import json
import os
import shutil
import torch

from net import BaseSupervisor
from net.tagger import BaseTagger, SequenceTagger
from resources import file_to_c_x_y, get_scoring_matrix
from settings import Settings
from tensorboardX import SummaryWriter
from tqdm import tqdm
from utilities.preprocessing import CCDDataSet


class SplitSupervisor(BaseSupervisor):
    """
    Supervisor for training- and evaluation-split processing.
    """

    def __init__(self, settings: Settings) -> None:
        """
        Initialise training- and evaluation-split supervisor.

        :param settings: settings dictionary to initialise from
        """
        self.__settings = settings
        self.__summary_writer = None
        self.__data_set_training = None
        self.__data_set_evaluation = None
        self.__tagger = None
        self.reset()

    def __del__(self) -> None:
        """
        Close summary writer instance.
        """
        if self.__summary_writer is not None and isinstance(self.__summary_writer, SummaryWriter):
            self.__summary_writer.close()

    def _prepare_directory(self, directory: str) -> None:
        """
        Prepare dictionary to be used by the supervisor.

        :param directory: dictionary to prepare
        """
        if os.path.exists(directory):
            if self.__settings.log.overwrite:
                shutil.rmtree(path=directory)
            else:
                raise FileExistsError(r'Target "{}" already existing! Aborting ...'.format(directory))
        os.makedirs(directory)

    def _create_data_set(self, training: bool = True) -> CCDDataSet:
        """
        Create data set either for training or evaluation.

        :param training: flag specifying if data set is for training or evaluation purposes
        :return: created data set
        """
        scoring_matrix = get_scoring_matrix(
            version=self.__settings.pre_processing.scoring_version,
            family=self.__settings.pre_processing.scoring_family,
            normalise=self.__settings.pre_processing.scoring_normalise)

        if training:
            file_path = self.__settings.data.training_file_path
            separator = self.__settings.data.training_sample_separator
            outlier_factor = self.__settings.pre_processing.outlier_factor
            distinct = self.__settings.data.training_distinct_samples
        else:
            file_path = self.__settings.data.evaluation_file_path
            separator = self.__settings.data.evaluation_sample_separator
            outlier_factor = -1
            distinct = self.__settings.data.evaluation_distinct_samples

        data_set = file_to_c_x_y(file_path=file_path, separator=separator, shuffle=False)
        return CCDDataSet.from_list(
            x=data_set.x, y=data_set.y, c=data_set.c,
            window_offset=self.__settings.pre_processing.window_offset,
            scoring_matrix=scoring_matrix,
            outlier_factor=outlier_factor,
            distinct=distinct)

    def _evaluate(self, global_step: int = None) -> None:
        """
        Evaluate and save checkpoint as well as evaluation summary of current model.

        :param global_step: global step of current evaluation
        """
        performance_training = self.__tagger.evaluate(
            data_set=self.__data_set_training,
            batch_size=self.__settings.run.batch_size)
        performance_evaluation = self.__tagger.evaluate(
            data_set=self.__data_set_evaluation,
            batch_size=self.__settings.run.batch_size)

        self.__summary_writer.add_scalars(
            main_tag=r'loss', tag_scalar_dict={
                r'training': performance_training.loss,
                r'evaluation': performance_evaluation.loss},
            global_step=global_step)
        self.__summary_writer.add_scalars(
            main_tag=r'precision', tag_scalar_dict={
                r'training': performance_training.precision,
                r'evaluation': performance_evaluation.precision},
            global_step=global_step)
        self.__summary_writer.add_scalars(
            main_tag=r'recall', tag_scalar_dict={
                r'training': performance_training.recall,
                r'evaluation': performance_evaluation.recall},
            global_step=global_step)
        self.__summary_writer.add_scalars(
            main_tag=r'specificity', tag_scalar_dict={
                r'training': performance_training.specificity,
                r'evaluation': performance_evaluation.specificity},
            global_step=global_step)
        self.__summary_writer.add_scalars(
            main_tag=r'f1score', tag_scalar_dict={
                r'training': performance_training.f1score,
                r'evaluation': performance_evaluation.f1score},
            global_step=global_step)
        self.__summary_writer.add_scalars(
            main_tag=r'balanced_accuracy', tag_scalar_dict={
                r'training': performance_training.balanced_accuracy,
                r'evaluation': performance_evaluation.balanced_accuracy},
            global_step=global_step)

        summary_file = os.path.join(
            self.__settings.log.summary_directory, r'training_epoch_{}.json'.format(global_step))
        with open(summary_file, mode=r'w') as summary_file:
            json.dump(obj=performance_training.to_dictionary(), fp=summary_file)

        summary_file = os.path.join(
            self.__settings.log.summary_directory, r'evaluation_epoch_{}.json'.format(global_step))
        with open(summary_file, mode=r'w') as summary_file:
            json.dump(obj=performance_evaluation.to_dictionary(), fp=summary_file)

    def operate(self) -> None:
        """
        Operate sequence tagger (training and evaluation).
        """
        for epoch in tqdm(range(1, self.__settings.run.epochs + 1, 1), desc=r'Progress', unit=r'ep'):

            # Train network for one epoch.
            self.__tagger.train(
                data_set=self.__data_set_training, num_epochs=1, batch_size=self.__settings.run.batch_size,
                shuffle=self.__settings.run.shuffle)

            # Save snapshot of current model.
            checkpoint_file = os.path.join(self.__settings.log.checkpoint_directory, r'epoch_{}.pt'.format(epoch))
            with open(checkpoint_file, mode=r'wb') as checkpoint_file:
                torch.save(obj=self.__tagger, f=checkpoint_file)

            # Evaluate network.
            if (epoch % self.__settings.run.evaluation_interval) != 0:
                continue
            self._evaluate(global_step=epoch)

    def reset(self) -> None:
        """
        Reset sequence tagger.
        """
        torch.manual_seed(seed=self.__settings.run.seed)

        self._prepare_directory(directory=self.__settings.log.checkpoint_directory)
        self._prepare_directory(directory=self.__settings.log.summary_directory)
        self.__summary_writer = SummaryWriter(log_dir=self.__settings.log.summary_directory)

        self.__data_set_training = self._create_data_set(training=True)
        self.__data_set_evaluation = self._create_data_set(training=False)

        if self.__tagger is not None and isinstance(self.__tagger, BaseTagger):
            self.__tagger.reset()
        else:
            self.__tagger = SequenceTagger(
                input_size=self.__data_set_training.input_dim,
                feature_size=self.__data_set_training.meta_dim,
                output_size=8, settings=self.__settings)
