import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional
import torch.nn.modules.rnn


class SequenceTaggerLocalityModule(nn.modules.Module):
    """
    Tagger for annotating a sequence 1:1 (locality module part).
    """

    def __init__(self, input_size: int, window_size: int, feature_size: int, input_dropout: float = 0,
                 extract_layers: int = 1, extract_size: int = None, extract_dropout: float = 0) -> None:
        """
        Initialise locality module of a 1:1 sequence tagger.

        :param input_size: depth of the input data
        :param window_size: offset specifying the amount of sequence positions added to both sides of the central one
        :param feature_size: depth of the feature vector
        :param input_dropout: dropout probability applied to disturb the input (including features)
        :param extract_layers: amount of extract layers to stack
        :param extract_size: depth of the context vectors of the extract layer (including transform processing unit)
        :param extract_dropout: dropout probability applied to disturb the data between extract layers
        """
        super(SequenceTaggerLocalityModule, self).__init__()

        assert input_size >= 1
        assert window_size >= 1
        assert feature_size >= 1
        assert extract_size >= 1
        assert extract_layers >= 1
        assert 0 <= input_dropout <= 1
        assert 0 <= extract_dropout <= 1

        self.input_size = input_size
        self.window_size = window_size
        self.feature_size = feature_size
        self.extract_size = extract_size
        self.extract_layers = extract_layers
        self.input_dropout_probability = input_dropout
        self.extract_dropout_probability = extract_dropout

        self.extract_states = None
        self.scan_states = None
        self._init_network()
        self.reset_parameters()

    def _init_network(self) -> None:
        """
        Initialise processing units of the locality module.
        """
        self.input_dropout = nn.Dropout(p=self.input_dropout_probability)
        self.extract = nn.LSTM(
            input_size=self.input_size, hidden_size=self.extract_size, num_layers=self.extract_layers, bias=True,
            batch_first=True, dropout=self.extract_dropout_probability, bidirectional=True)

        self.__initialiser_size = self.extract.hidden_size * 2 * self.extract.num_layers
        self.extract_fw_initialiser = nn.Linear(
            in_features=self.__initialiser_size, out_features=self.__initialiser_size)
        self.extract_bw_initialiser = nn.Linear(
            in_features=self.__initialiser_size, out_features=self.__initialiser_size)

        self.transform = nn.Linear(
            in_features=self.feature_size, out_features=self.extract_size, bias=True)

    def reset_parameters(self) -> None:
        """
        Reset parameters of the locality module.
        """
        for name, parameter in self.extract.named_parameters():
            if r'bias' in name:
                parameter_length = len(parameter.data)
                nn.init.constant_(tensor=parameter.data[:parameter_length // 4], val=0)
                nn.init.constant_(tensor=parameter.data[parameter_length // 4:parameter_length // 2], val=0)
                nn.init.constant_(tensor=parameter.data[parameter_length // 2:int(parameter_length * 0.75)], val=0)
                nn.init.constant_(tensor=parameter.data[int(parameter_length * 0.75):], val=0)
            elif r'weight' in name:
                nn.init.xavier_normal_(parameter.data)

        nn.init.constant_(tensor=self.extract_fw_initialiser.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.extract_fw_initialiser.weight.data)

        nn.init.constant_(tensor=self.extract_bw_initialiser.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.extract_bw_initialiser.weight.data)

        nn.init.constant_(tensor=self.transform.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.transform.weight.data)

    def forward(self, x: torch.Tensor, features: torch.Tensor) -> torch.Tensor:
        """
        Execute forward pass of the locality module, computing localities extracted from current sequences.

        :param x: sequences to analyse
        :param features: manually extracted features with respect to 'x'
        :return: extracted localities with respect to 'x'
        """
        base_shape = x.shape[:2]
        x = x.view(np.multiply(*x.shape[:2]).tolist(), x.shape[-1] // self.input_size, self.input_size)
        features = features.view(np.multiply(*features.shape[:2]).tolist(), self.feature_size)

        x = self.input_dropout(input=x)
        features = self.input_dropout(input=features)

        basis = torch.ones(self.__initialiser_size).to(device=x.device)
        chunk_count = 2 * self.extract.num_layers
        extract_states_fw = nn.functional.tanh(input=self.extract_fw_initialiser(input=basis)).chunk(chunks=chunk_count)
        extract_states_bw = nn.functional.tanh(input=self.extract_bw_initialiser(input=basis)).chunk(chunks=chunk_count)
        extract_states = (
            torch.cat((*(_.unsqueeze(dim=0) for _ in extract_states_fw[:self.extract.num_layers]),
                       *(_.unsqueeze(dim=0) for _ in extract_states_bw[:self.extract.num_layers])), dim=0).unsqueeze(
                dim=1).expand(-1, len(x), -1).contiguous(),
            torch.cat((*(_.unsqueeze(dim=0) for _ in extract_states_fw[self.extract.num_layers:]),
                       *(_.unsqueeze(dim=0) for _ in extract_states_bw[self.extract.num_layers:])), dim=0).unsqueeze(
                dim=1).expand(-1, len(x), -1).contiguous())

        extracted_data = self.extract(input=x, hx=extract_states)[0]
        extracted_data_fw = extracted_data[:, -1, :self.extract.hidden_size].contiguous()
        extracted_data_bw = extracted_data[:, 0, self.extract.hidden_size:].contiguous()

        transformed_data = self.transform(input=features)
        transformed_data = nn.functional.tanh(input=transformed_data)
        return torch.cat((extracted_data_fw, extracted_data_bw, transformed_data), dim=-1).contiguous().view(
            *base_shape, self.extract.hidden_size * 2 + self.transform.out_features)


class SequenceTaggerGlobalityModule(nn.modules.Module):
    """
    Tagger for annotating a sequence 1:1 (globality module part).
    """

    def __init__(self, input_size: int, scan_size: int, output_size: int, scan_layers: int = 1,
                 scan_dropout: float = 0, project_dropout: float = 0) -> None:
        """
        Initialise globality module of a 1:1 sequence tagger.

        :param input_size: depth of the input data
        :param scan_size: depth of the context vectors of the scan layer
        :param output_size: depth of the output projection
        :param scan_layers: amount of scan layers to stack
        :param scan_dropout: dropout probability applied to disturb the data between scan layers
        :param project_dropout: dropout probability applied to disturb the output (before projection)
        """
        super(SequenceTaggerGlobalityModule, self).__init__()

        assert input_size >= 1
        assert scan_size >= 1
        assert scan_layers >= 1
        assert output_size >= 1
        assert 0 <= scan_dropout <= 1
        assert 0 <= project_dropout <= 1

        self.input_size = input_size
        self.scan_size = scan_size
        self.scan_layers = scan_layers
        self.output_size = output_size
        self.scan_dropout_probability = scan_dropout
        self.project_dropout_probability = project_dropout

        self.extract_states = None
        self.scan_states = None
        self._init_network()
        self.reset_parameters()

    def _init_network(self) -> None:
        """
        Initialise processing units of the globality module.
        """
        self.scan = nn.LSTM(
            input_size=self.input_size, hidden_size=self.scan_size, num_layers=self.scan_layers,
            bias=True, batch_first=True, dropout=self.scan_dropout_probability, bidirectional=True)

        self.__initialiser_size = self.scan.hidden_size * 2 * self.scan.num_layers
        self.scan_fw_initialiser = nn.Linear(
            in_features=self.__initialiser_size, out_features=self.__initialiser_size)
        self.scan_bw_initialiser = nn.Linear(
            in_features=self.__initialiser_size, out_features=self.__initialiser_size)

        self.project_dropout = nn.Dropout(p=self.project_dropout_probability)
        self.project = nn.Linear(in_features=self.scan.hidden_size * 2, out_features=self.output_size, bias=True)

    def reset_parameters(self) -> None:
        """
        Reset parameters of the globality module.
        """
        for name, parameter in self.scan.named_parameters():
            if r'bias' in name:
                parameter_length = len(parameter.data)
                nn.init.constant_(tensor=parameter.data[:parameter_length // 4], val=-4)
                nn.init.constant_(tensor=parameter.data[parameter_length // 4:parameter_length // 2], val=4)
                nn.init.constant_(tensor=parameter.data[parameter_length // 2:int(parameter_length * 0.75)], val=0)
                nn.init.constant_(tensor=parameter.data[int(parameter_length * 0.75):], val=0)
            elif r'weight' in name:
                nn.init.xavier_normal_(parameter.data)

        nn.init.constant_(tensor=self.scan_fw_initialiser.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.scan_fw_initialiser.weight.data)

        nn.init.constant_(tensor=self.scan_bw_initialiser.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.scan_bw_initialiser.weight.data)

        nn.init.constant_(tensor=self.project.bias.data, val=0)
        nn.init.xavier_normal_(tensor=self.project.weight.data)

    def forward(self, x: torch.Tensor, lengths: torch.Tensor) -> torch.Tensor:
        """
        Execute forward pass of the globality module, putting localities into a global context.

        :param x: localities to analyse
        :param lengths: lengths of the locality sequences
        :return: extracted globalities with respect to 'x'
        """
        basis = torch.ones(self.__initialiser_size).to(device=x.device)
        chunk_count = 2 * self.scan.num_layers
        scan_states_fw = nn.functional.tanh(input=self.scan_fw_initialiser(input=basis)).chunk(chunks=chunk_count)
        scan_states_bw = nn.functional.tanh(input=self.scan_bw_initialiser(input=basis)).chunk(chunks=chunk_count)
        self.scan_states = (
            torch.cat((*(_.unsqueeze(dim=0) for _ in scan_states_fw[:self.scan.num_layers]),
                       *(_.unsqueeze(dim=0) for _ in scan_states_bw[:self.scan.num_layers])), dim=0).unsqueeze(
                dim=1).expand(-1, len(lengths), -1).contiguous(),
            torch.cat((*(_.unsqueeze(dim=0) for _ in scan_states_fw[self.scan.num_layers:]),
                       *(_.unsqueeze(dim=0) for _ in scan_states_bw[self.scan.num_layers:])), dim=0).unsqueeze(
                dim=1).expand(-1, len(lengths), -1).contiguous())

        x = nn.utils.rnn.pack_padded_sequence(x, lengths=lengths, batch_first=True)
        scan_out = self.scan(input=x, hx=self.scan_states)[0]
        scan_out = nn.utils.rnn.pad_packed_sequence(scan_out, batch_first=True, padding_value=0.0)[0].contiguous()

        project_out = self.project(input=self.project_dropout(input=scan_out.view(-1, scan_out.shape[-1])))
        return project_out.view(*scan_out.shape[:2], self.project.out_features)
