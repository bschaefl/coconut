import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional
import torch.optim as optim

from itertools import chain
from net import BaseTagger
from net.modules import SequenceTaggerLocalityModule, SequenceTaggerGlobalityModule
from settings import Settings
from sklearn.metrics import confusion_matrix
from torch.multiprocessing import cpu_count
from torch.utils.data import DataLoader
from typing import Tuple
from utilities.metrics import PerformanceEntry
from utilities.preprocessing import CCDDataSet


class SequenceTagger(BaseTagger):
    """
    Tagger for annotating a sequence 1:1.
    """

    def __init__(self, input_size: int, feature_size: int, output_size: int, settings: Settings) -> None:
        """
        Initialise sequence tagger.

        :param input_size: depth of the input data
        :param feature_size: depth of the feature vector
        :param output_size: depth of the output projection
        :param settings: settings to initialise from
        """
        self.__input_size = input_size
        self.__feature_size = feature_size
        self.__output_size = output_size
        self.__settings = settings
        self.__class_weights = None
        self.__device = r'cuda' if torch.cuda.is_available() and self.__settings.run.cuda else r'cpu'

        self._init_tagger_module()
        self._init_optimiser()

    def _init_tagger_module(self) -> None:
        """
        Initialise modules of sequence tagger.
        """
        self.__locality_tagger_module = SequenceTaggerLocalityModule(
            input_size=self.__input_size,
            window_size=self.__settings.pre_processing.window_offset * 2 + 1,
            feature_size=self.__feature_size,
            extract_size=self.__settings.architecture.extract_size,
            extract_layers=self.__settings.architecture.extract_layers,
            input_dropout=self.__settings.architecture.input_dropout,
            extract_dropout=self.__settings.architecture.extract_dropout)
        self.__globality_tagger_module = SequenceTaggerGlobalityModule(
            input_size=self.__settings.architecture.extract_size * 3,
            scan_size=self.__settings.architecture.scan_size,
            scan_layers=self.__settings.architecture.scan_layers,
            output_size=self.__output_size,
            project_dropout=self.__settings.architecture.project_dropout)
        self.__locality_tagger_module = self.__locality_tagger_module.to(self.__device)
        self.__globality_tagger_module = self.__globality_tagger_module.to(self.__device)

    def _init_optimiser(self) -> None:
        """
        Initialise optimiser of all sequence tagger modules.
        """
        self.__optimiser = optim.Adam((
            {r'params': self.__locality_tagger_module.parameters()},
            {r'params': self.__globality_tagger_module.parameters()}),
            lr=self.__settings.optimiser.learning_rate,
            betas=(self.__settings.optimiser.beta1, self.__settings.optimiser.beta2),
            eps=self.__settings.optimiser.epsilon,
            weight_decay=self.__settings.optimiser.weight_decay,
            amsgrad=self.__settings.optimiser.amsgrad)

    def _init_class_weights(self, data_set: CCDDataSet) -> None:
        """
        Initialise heptad position label weights according to underlying data set.

        :param data_set: data set to be used to fetch heptad position label weights
        """
        if self.__class_weights is None:
            if self.__settings.run.weight_classes:
                self.__class_weights = data_set.class_weights(
                    normalise=self.__settings.run.weight_classes_normalised,
                    deleted=self.__settings.run.weight_classes_deleted)
                self.__class_weights = torch.from_numpy(self.__class_weights).to(dtype=torch.float32)
            else:
                self.__class_weights = torch.ones(self.__output_size, dtype=torch.float32)
            self.__class_weights = self.__class_weights.to(device=self.__device)

    def train(self, data_set: CCDDataSet, num_epochs: int, batch_size: int, shuffle: bool = False) -> None:
        """
        Train the network by forward- and backward pass pairs.

        :param data_set: data set to be used for training
        :param num_epochs: amount of epochs to train
        :param batch_size: amount of sequences to be used per forward iteration
        :param shuffle: shuffle data set at the beginning of each epoch
        """
        # Set network into training mode and initialise data loader.
        self.__locality_tagger_module.train()
        self.__globality_tagger_module.train()
        data_loader = DataLoader(
            dataset=data_set, batch_size=batch_size, shuffle=shuffle, num_workers=cpu_count(),
            pin_memory=True, drop_last=False)

        # Compute class weights.
        self._init_class_weights(data_set=data_set)

        # Iterate over data set and train network.
        for epoch in range(1, num_epochs + 1, 1):
            for x, y, lengths in data_loader:
                _, indices_sorting = torch.sort(input=lengths, descending=True)

                f = x[indices_sorting, :lengths.max(), -data_set.meta_dim:].to(dtype=torch.float32).contiguous()
                x = x[indices_sorting, :lengths.max(), :-data_set.meta_dim].to(dtype=torch.float32).contiguous()
                y = y[indices_sorting, :lengths.max()].to(dtype=torch.long).contiguous()

                f, x, y = f.to(device=self.__device), x.to(device=self.__device), y.to(device=self.__device)
                lengths = lengths[indices_sorting]

                network_locality_out = self.__locality_tagger_module(x=x, features=f)
                network_globality_out = self.__globality_tagger_module(x=network_locality_out, lengths=lengths)

                network_globality_out = network_globality_out.view(
                    np.multiply(*network_globality_out.shape[:2]).tolist(), network_globality_out.shape[-1])
                batch_loss = nn.functional.cross_entropy(
                    input=network_globality_out, target=y.view(-1), weight=self.__class_weights,
                    ignore_index=data_set.padding_sign_y)

                self.__optimiser.zero_grad()
                batch_loss.backward()
                self.__optimiser.step()

        # Clean up gradient state.
        self.__optimiser.zero_grad()

    def predict(self, data_set: CCDDataSet, batch_size: int, decode: bool = True,
                track_loss: bool = False) -> Tuple[list, float]:
        """
        Predict heptad position labels for a given data set.

        :param data_set: data set to be used for prediction
        :param batch_size: amount of sequences to be used per forward iteration
        :param decode: decode integer representation of heptad position labels
        :param track_loss: track loss of current model (corresponding 'y' must be specified)
        :return: predicted heptad position labels as well as loss of current model (if specified)
        """
        # Set network into evaluation mode and initialise data loader.
        self.__locality_tagger_module.eval()
        self.__globality_tagger_module.eval()
        data_loader = DataLoader(
            dataset=data_set, batch_size=batch_size, shuffle=False, num_workers=cpu_count(),
            pin_memory=True, drop_last=False)

        # Compute class weights.
        self._init_class_weights(data_set=data_set)

        # Iterate over data set and predict the corresponding targets.
        predictions, losses = [], []
        for current_batch in data_loader:
            x, lengths = current_batch[0], current_batch[-1]
            _, indices_sorting = torch.sort(input=lengths, descending=True)

            f = x[indices_sorting, :lengths.max(), -data_set.meta_dim:].to(dtype=torch.float32).contiguous()
            x = x[indices_sorting, :lengths.max(), :-data_set.meta_dim].to(dtype=torch.float32).contiguous()
            if len(current_batch) == 3:
                y = current_batch[1]
                y = y[indices_sorting, :lengths.max()].to(dtype=torch.long).contiguous().to(device=self.__device)

            f, x = f.to(device=self.__device), x.to(device=self.__device)
            lengths = lengths[indices_sorting]

            network_out_mask = torch.arange(lengths.max()).repeat(len(lengths), 1).to(dtype=torch.long)
            network_out_mask = (network_out_mask < lengths.unsqueeze(dim=-1)).to(device=self.__device)
            network_locality_out = self.__locality_tagger_module(x=x, features=f)
            network_globality_out = self.__globality_tagger_module(x=network_locality_out, lengths=lengths)

            if track_loss:
                network_globality_out = network_globality_out.view(
                    np.multiply(*network_globality_out.shape[:2]).tolist(), network_globality_out.shape[-1])
                batch_loss = nn.functional.cross_entropy(
                    input=network_globality_out, target=y.view(-1), ignore_index=data_set.padding_sign_y)
                losses.append(batch_loss.item())
                network_globality_out = network_globality_out.view(len(lengths), lengths.max(), self.__output_size)

            current_probabilities = nn.functional.softmax(input=network_globality_out, dim=-1)
            current_predictions = current_probabilities.max(dim=-1)[1].masked_select(mask=network_out_mask)
            current_predictions = current_predictions.to(device=r'cpu').numpy().astype(dtype=np.int32)

            if decode:
                current_predictions = np.expand_dims(current_predictions, axis=1)
                current_predictions = np.apply_along_axis(data_set.int_to_heptad, axis=1, arr=current_predictions)

            current_predictions = map(list, np.split(current_predictions, indices_or_sections=np.cumsum(lengths)))
            current_predictions = list(filter(bool, current_predictions))

            _, indices_sorting = torch.sort(input=indices_sorting, descending=False)
            current_predictions = [current_predictions[_] for _ in indices_sorting]
            predictions.extend(current_predictions)

        return predictions, sum(losses) / len(losses) if track_loss else None

    def evaluate(self, data_set: CCDDataSet, batch_size: int) -> PerformanceEntry:
        """
        Evaluate current model.

        :param data_set: data set to be used for evaluation
        :param batch_size: amount of sequences to be used per forward iteration
        :return: performance of current model
        """
        predictions, loss = self.predict(data_set=data_set, batch_size=batch_size, decode=False, track_loss=True)
        predictions = np.asarray(list(chain.from_iterable(predictions)), dtype=np.int32)
        target = torch.from_numpy(data_set.y.astype(np.int32))

        target_mask = (target < data_set.padding_sign_y).to(dtype=torch.uint8)
        target = target.masked_select(mask=target_mask).numpy().astype(dtype=np.int32)

        labels = list(range(data_set.output_dim))
        computed_confusion_matrix = confusion_matrix(y_true=target, y_pred=predictions, labels=labels)

        performance = PerformanceEntry(num_classes=len(labels))
        performance.update_confusion_matrix(other=computed_confusion_matrix)
        performance.update_loss(other=loss)
        return performance

    def reset(self) -> None:
        """
        Reset sequence tagger.
        """
        self._init_tagger_module()
        self._init_optimiser()
        self.__class_weights = None
