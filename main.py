#!/usr/bin/env python3
import argparse
import os
import torch

from net.supervisors import SplitSupervisor
from resources import get_scoring_matrix
from settings import Settings
from utilities.preprocessing import CCDDataSet

if __name__ == r'__main__':
    arg_parser = argparse.ArgumentParser(description=r'CoCoNuT predicts occurrences of CCDs in protein sequences.')
    arg_sub_parsers = arg_parser.add_subparsers()
    arg_sub_parsers.required = True
    arg_sub_parsers.dest = r'mode'

    train_parser = arg_sub_parsers.add_parser(name=r'train', help=r'train network using specified settings file')
    train_parser.add_argument(r'-s', r'--settings', type=str, help=r'settings file (json) to use', required=True)

    predict_parser = arg_sub_parsers.add_parser(name=r'predict', help=r'predict CCDs using specified checkpoint')
    predict_parser.add_argument(r'-c', r'--checkpoint', type=str, help=r'checkpoint file (pt) to use', required=True)
    predict_parser.add_argument(r'-a', r'--aminoacids', type=str, help=r'AA sequence to analyse', required=True)

    args = arg_parser.parse_args()

    # Execute CoCoNuT.
    if args.mode == r'train':

        # Train and optionally evaluate model.
        coconut_settings = Settings.from_json_file(file=args.settings)
        supervisor = SplitSupervisor(settings=coconut_settings)
        supervisor.operate()

    elif args.mode == r'predict':

        # Load and cleanup model.
        coconut = torch.load(f=args.checkpoint, map_location=r'cpu')
        coconut._SequenceTagger__device = r'cpu'
        coconut._SequenceTagger__optimiser = None

        # Load and prepare sequence data to analyse.
        settings = coconut._SequenceTagger__settings
        scoring_matrix = get_scoring_matrix(
            version=settings.pre_processing.scoring_version,
            family=settings.pre_processing.scoring_family,
            normalise=settings.pre_processing.scoring_normalise)
        sequences = CCDDataSet.from_list(
            x=[args.aminoacids],
            window_offset=settings.pre_processing.window_offset,
            scoring_matrix=scoring_matrix,
            outlier_factor=-1,
            distinct=False)
        result = coconut.predict(data_set=sequences, batch_size=1, decode=True, track_loss=False)[0]
        print(r'{}'.format(os.linesep).join(r''.join(_) for _ in result))

    else:
        raise ValueError(r'Invalid mode specified! Aborting...')
