import json
import numpy as np
import os
import shutil
import tempfile
import torch.utils.data as data

from Bio.Alphabet.IUPAC import IUPACProtein
from Bio.SeqUtils import ProtParamData
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from collections import Counter
from functools import lru_cache
from itertools import chain, repeat, product
from random import choices
from torch.multiprocessing import current_process, Pool
from typing import Dict, List, Tuple, Union


class CCDDataSet(data.Dataset):
    """
    Class representing a data set.
    """

    def __init__(self) -> None:
        """
        Initialise empty data set.
        """
        self.__input_alphabet = IUPACProtein.letters
        self.__output_alphabet = [
            r'-', r'a', r'b', r'c', r'd', r'e', r'f', r'g'
        ]

        self.__window_offset = None
        self.__window_size = None
        self.__input_dim = None
        self.__meta_dim = None
        self.__output_dim = None
        self.__outlier_factor = None
        self.__distinct = None
        self.__max_length = None
        self.__sequence_indices = None
        self.__sequence_clusters = None
        self.__checkpoint_dir = None
        self.__checkpoint_volatile = None

    def __del__(self) -> None:
        """
        Delete temporary directory, if created automatically as part of the current data set.
        """
        if current_process().name == r'MainProcess' and self.__checkpoint_dir is not None and os.path.isdir(
                self.__checkpoint_dir) and self.__checkpoint_volatile:
            shutil.rmtree(path=self.__checkpoint_dir)

    def __len__(self) -> int:
        """
        Compute the size of the data set (with respect to currently active indices).

        :return: size of the data set
        """
        return len(self.__sequence_indices)

    def __getitem__(self, index) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Fetch an entry from the data set specified by its index.

        :param index: index of the entry to fetch
        :return: entry fetched from the data set
        """
        mapped_index = self.__sequence_indices[index]
        mapped_cluster = self.__sequence_clusters[mapped_index]
        mapped_dir = os.path.join(self.__checkpoint_dir, str(mapped_cluster))
        data_set = {
            r'x': np.load(file=os.path.join(mapped_dir, r'{}_x.npy'.format(mapped_index))),
            r'meta': np.load(file=os.path.join(mapped_dir, r'{}_m.npy'.format(mapped_index))),
            r'lengths': np.load(file=os.path.join(mapped_dir, r'{}_l.npy'.format(mapped_index)))}

        # Pad sequences.
        padded_x = np.pad(
            data_set[r'x'], pad_width=(
                (self.__window_offset, self.__max_length - data_set[r'x'].shape[0] + self.__window_offset), (0, 0)),
            mode=r'constant', constant_values=self.padding_sign_x)

        try:
            data_set[r'y'] = np.load(file=os.path.join(mapped_dir, r'{}_y.npy'.format(mapped_index)))
            padded_y = np.pad(
                data_set[r'y'].squeeze(), pad_width=(0, self.__max_length - data_set[r'y'].shape[0]),
                mode=r'constant', constant_values=self.padding_sign_y)
        except (IOError, ValueError):
            padded_y = None

        # Add encoded amino acid window.
        x_windowed = np.zeros(
            (self.__max_length, data_set[r'x'].shape[1] * self.__window_size + self.__meta_dim),
            dtype=data_set[r'x'].dtype)

        for i in range(x_windowed.shape[0]):
            current_window = padded_x[i:i + self.__window_size, :]
            current_window_border = np.multiply(*current_window.shape)
            x_windowed[i, :current_window_border] = current_window.flatten()

        # Add physical properties of sequence window.
        x_windowed[:data_set[r'meta'].shape[0], -self.__meta_dim:] = data_set[r'meta']

        return (x_windowed, data_set[r'lengths']) if padded_y is None else (x_windowed, padded_y, data_set[r'lengths'])

    def _create_code_dictionary(self) -> None:
        """
        Create code dictionary used for statically embedding amino acids.
        """
        if self.__scoring_matrix is None:
            self.__scoring_matrix = {
                a: {aa: (1 if a == aa else 0) for aa in self.__input_alphabet} for a in self.__input_alphabet}

        self.__CODE = {
            a: [self.__scoring_matrix[a][_] for _ in sorted(self.__scoring_matrix[a])]
            for a in sorted(self.__scoring_matrix) if a in self.__input_alphabet
        }
        self.__CODE.update({k: [float(v)] for k, v in self.__HEPTAD_TO_INT.items()})

    def _create_heptad_dictionary(self) -> None:
        """
        Create heptad dictionary used for translating heptad position labels to integers and vice versa.
        """
        self.__INT_TO_HEPTAD = {
            i: h for i, h in enumerate(self.__output_alphabet)
        }

        self.__HEPTAD_TO_INT = {
            self.__INT_TO_HEPTAD[i]: i for i in self.__INT_TO_HEPTAD
        }

    def _compute_physical_properties(self, sequence: str, window: int, sequence_length: int,
                                     normalise: bool = False) -> np.ndarray:
        """
        Compute physical and biochemical properties of specified strand of amino acids.

        :param sequence: strand of which to compute properties
        :param window: position of the strand with respect to the originating sequence
        :param sequence_length: length of originating sequence
        :param normalise: normalise properties to be in the interval [-1;1]
        :return: computed physical and biochemical properties
        """
        if len(sequence) > 1:
            analysis = ProteinAnalysis(prot_sequence=sequence)
            properties = np.asarray((
                analysis.protein_scale(param_dict=ProtParamData.kd, window=len(sequence)).pop(),
                analysis.protein_scale(param_dict=ProtParamData.hw, window=len(sequence)).pop(),
                analysis.protein_scale(param_dict=ProtParamData.em, window=len(sequence)).pop(),
                analysis.protein_scale(param_dict=ProtParamData.ja, window=len(sequence)).pop(),
                analysis.protein_scale(param_dict=ProtParamData.Flex, window=len(sequence)).pop(),
                analysis.instability_index(),
                analysis.isoelectric_point(),
                analysis.aromaticity(),
                window
            ), dtype=np.float32)

            if normalise:
                min_values = np.asarray([
                    np.min(list(ProtParamData.kd.values())),
                    np.min(list(ProtParamData.hw.values())),
                    np.min(list(ProtParamData.em.values())),
                    np.min(list(ProtParamData.ja.values())),
                    np.min(list(ProtParamData.Flex.values())),
                    (10 / len(sequence)) * np.min([
                        list(_.values()) for _ in ProtParamData.DIWV.values()]) * (len(sequence) - 1),
                    0,
                    0,
                    0
                ], dtype=np.float32)
                max_values = np.asarray([
                    np.max(list(ProtParamData.kd.values())),
                    np.max(list(ProtParamData.hw.values())),
                    np.max(list(ProtParamData.em.values())),
                    np.max(list(ProtParamData.ja.values())),
                    np.max(list(ProtParamData.Flex.values())),
                    (10 / len(sequence)) * np.max([
                        list(_.values()) for _ in ProtParamData.DIWV.values()]) * (len(sequence) - 1),
                    14,
                    1,
                    sequence_length - 1
                ], dtype=np.float32)
                properties = 2 * (properties - min_values) / (max_values - min_values) - 1
        else:
            properties = np.zeros(9, dtype=np.float32)

        return properties

    def _analyse_sequence(self, sequence: str, normalise: bool = True) -> np.ndarray:
        """
        Analyse sequence with respect to precomputed properties in a rolling window manner.

        :param sequence: sequence to analyse
        :param normalise: normalise properties to be in the interval [-1;1]
        :return: analysis results of current sequence
        """
        return np.asarray([self._compute_physical_properties(
            sequence=sequence[max(0, i - self.__window_offset):min(len(sequence), i + self.__window_offset + 1)],
            window=i, sequence_length=len(sequence), normalise=normalise)
            for i, _ in enumerate(sequence)], dtype=np.float32)

    def _encode_sequences(self, sequences: List[str]) -> List[np.ndarray]:
        """
        Statically embed amino acids of all sequences.

        :param sequences: sequences to embed
        :return: statically embedded sequences
        """
        return [np.asarray([self.__CODE[_] for _ in sequence], dtype=np.float32) for sequence in sequences]

    def _remove_outlier(self, x: List[str], y: List[str] = None,
                        c: List[str] = None) -> Tuple[List[str], List[str], List[str]]:
        """
        Remove outliers with respect to sequence length.

        :param x: part I of sequences (e.g. amino acids)
        :param y: part II of sequences (e.g. heptad annotations)
        :param c: part III of sequences (e.g. cluster affiliation)
        :return: pruned sequences, including all initally specified parts
        """
        lengths = [len(_) for _ in x]
        quartiles = (np.percentile(a=lengths, q=25, interpolation=r'midpoint'),
                     np.percentile(a=lengths, q=75, interpolation=r'midpoint'))
        outlier = np.abs(np.subtract(*quartiles)) * self.__outlier_factor
        indices = [i for i, _ in enumerate(lengths) if (quartiles[0] - outlier) <= _ <= (quartiles[1] + outlier)]

        data_set = {r'x': [], r'y': [] if y else None, r'c': [] if c else None}
        for index in indices:
            data_set[r'x'].append(x[index])
            if y is not None:
                data_set[r'y'].append(y[index])
            if c is not None:
                data_set[r'c'].append(c[index])

        return data_set[r'x'], data_set[r'y'], data_set[r'c']

    def _fetch_and_preprocess_data(self, x: List[str], y: List[str] = None, c: List[str] = None):
        """
        Fetch and preprocess sequences.

        :param x: part I of sequences (e.g. amino acids)
        :param y: part II of sequences (e.g. heptad annotations)
        :param c: part III of sequences (e.g. cluster affiliation)
        :return: preprocessed sequences
        """
        # Remove duplicate entries.
        if self.__distinct:
            _, indices = np.unique(x, return_index=True)
            x = [x[_] for _ in indices]
            if y is not None:
                y = [y[_] for _ in indices]
            if c is not None:
                c = [c[_] for _ in indices]

        # Remove outlier (according to sequence length).
        if self.__outlier_factor >= 0:
            data_set = self._remove_outlier(x=x, y=y, c=c)
        else:
            data_set = (x, y, c)
        data_set = {k: v for k, v in zip((r'x', r'y', r'c'), data_set)}

        # Compute sequence lengths and cluster.
        data_set[r'lengths'] = [len(_) for _ in data_set[r'x']]
        self.__max_length = max(data_set[r'lengths'])
        self.__sequence_indices_original = list(range(len(data_set[r'x'])))
        data_set[r'c'] = np.asarray(self.__sequence_indices_original if c is None else data_set[r'c'], dtype=np.int32)
        self.__sequence_clusters = data_set[r'c'].tolist()
        for c_dir in set(self.__sequence_clusters):
            os.mkdir(os.path.join(self.__checkpoint_dir, str(c_dir)))

        # Compute physical properties of sequences.
        with Pool() as worker_pool:
            data_set[r'meta'] = worker_pool.map(self._analyse_sequence, data_set[r'x'])

        # Encode input and target sequences.
        data_set[r'x'] = self._encode_sequences(sequences=data_set[r'x'])
        data_set[r'y'] = [None] * len(data_set[r'c']) if y is None else self._encode_sequences(sequences=data_set[r'y'])

        # Write preprocessed sequences to disk (more precisely, to the system's temporary folder).
        for x_file, y_file, meta_file, lengths_file, c_dir, index in zip(
                data_set[r'x'], data_set[r'y'], data_set[r'meta'],
                data_set[r'lengths'], data_set[r'c'], self.__sequence_indices_original):
            np.save(file=os.path.join(self.__checkpoint_dir, str(c_dir), r'{}_x'.format(str(index))), arr=x_file)
            np.save(file=os.path.join(self.__checkpoint_dir, str(c_dir), r'{}_m'.format(str(index))), arr=meta_file)
            np.save(file=os.path.join(self.__checkpoint_dir, str(c_dir), r'{}_l'.format(str(index))), arr=lengths_file)
            if y_file is not None:
                np.save(file=os.path.join(self.__checkpoint_dir, str(c_dir), r'{}_y'.format(str(index))), arr=y_file)

    def _count_heptad_frequencies(self, sequence: np.ndarray) -> Counter:
        """
        Compute frequency dictionary of all heptad position labels of a given sequence.

        :param sequence: sequence of which to compute heptad position label frequency dictionary
        :return: heptad position label frequency dictionary
        """
        counts = {_: 0 for _ in [c for c in self.__INT_TO_HEPTAD] + [self.padding_sign_y]}
        for label in sequence.flat:
            counts[label] += 1
        return Counter(counts)

    def _reset_indices(self) -> None:
        """
        Reset currently active indices to represent all initial sequences.
        """
        self.__sequence_indices = self.__sequence_indices_original

    @classmethod
    def from_list(cls, x: List[str], y: List[str] = None, c: List[str] = None, window_offset: int = 4,
                  scoring_matrix: dict = None, outlier_factor: float = -1, distinct: bool = False) -> r'CCDDataSet':
        """
        Initialise data set from specified sequences in list form.

        :param x: part I of sequences (e.g. amino acids)
        :param y: part II of sequences (e.g. heptad annotations)
        :param c: part III of sequences (e.g. cluster affiliation)
        :param window_offset: offset specifying the amount of sequence positions added to both sides of the central one
        :param scoring_matrix: scoring matrix to use for the statical embedding of the amino acids
        :param outlier_factor: factor specifying the threshold to label a sequence as outlier
        :param distinct: use only unique sequences with respect to 'x' (part I)
        :return: initialised data set
        """
        data_set = cls()
        data_set.__window_offset = window_offset
        data_set.__window_size = data_set.__window_offset * 2 + 1
        data_set.__meta_dim = len(data_set._compute_physical_properties(
            sequence=data_set.__input_alphabet[:data_set.__window_size], window=0,
            sequence_length=len(data_set.__input_alphabet[:data_set.__window_size])))

        data_set.__outlier_factor = outlier_factor
        data_set.__distinct = distinct
        data_set.__scoring_matrix = scoring_matrix

        data_set._create_heptad_dictionary()
        data_set._create_code_dictionary()
        data_set.__input_dim = len([_ for _ in data_set.__CODE if _.isupper()])
        data_set.__output_dim = len([_ for _ in data_set.__HEPTAD_TO_INT])

        data_set.__checkpoint_dir = tempfile.mkdtemp()
        data_set.__checkpoint_volatile = True
        data_set._fetch_and_preprocess_data(x=x, y=y, c=c)
        data_set.__sequence_indices = None
        data_set._reset_indices()

        assert data_set.__input_dim == 20, r'Wrong input dimensionality due to deficient scoring matrix!'
        assert data_set.__output_dim == 8, r'Wrong output dimensionality due to deficient translation dictionary!'

        return data_set

    @classmethod
    def from_checkpoint(cls, checkpoint_dir: str) -> r'CCDDataSet':
        """
        Initialise data set from a previously saved checkpoint.

        :param checkpoint_dir: directory which contains the checkpoint
        :return: initialised data set
        """
        data_set = cls()
        data_set.load(checkpoint_dir=checkpoint_dir)

        return data_set

    def save(self, checkpoint_dir: str) -> None:
        """
        Save current data set creating a corresponding checkpoint.

        :param checkpoint_dir: directory for storing the checkpoint
        """
        if os.path.exists(checkpoint_dir):
            raise FileExistsError(r'Target checkpoint directory already exists! Aborting ...')

        shutil.copytree(src=self.__checkpoint_dir, dst=checkpoint_dir)
        with open(os.path.join(checkpoint_dir, r'settings.json'), mode=r'w') as settings_file:
            settings = {
                k: v for k, v in self.__dict__.items() if r'checkpoint_dir' not in k and not type(v) is np.ndarray}
            json.dump(settings, fp=settings_file)

    def load(self, checkpoint_dir: str) -> None:
        """
        Load data set from a previously saved checkpoint by overwriting the current data set. Note, that this operation
        is not recommended if the current data set is not empty and temporary files were automatically created, as these
        do not get removed anymore!

        :param checkpoint_dir: directory which contains the checkpoint
        """
        if not os.path.exists(checkpoint_dir):
            raise FileExistsError(r'Target checkpoint directory does not exist! Aborting ...')

        with open(os.path.join(checkpoint_dir, r'settings.json'), mode=r'r') as settings_file:
            self.__dict__ = json.load(fp=settings_file)
            self.__checkpoint_dir = checkpoint_dir
            self.__checkpoint_volatile = False
            self._create_heptad_dictionary()
            self._create_code_dictionary()

    def heptad_to_int(self, to_int: str) -> int:
        """
        Translate heptad position label to corresponding integer encoding.

        :param to_int: heptad position label to translate
        :return: translated heptad position label
        """
        return self.__HEPTAD_TO_INT[str(to_int)]

    def int_to_heptad(self, to_heptad: Union[int, np.ndarray]) -> str:
        """
        Translate integer encoding to corresponding heptad position label.

        :param to_heptad: integer encoding to translate
        :return: translated integer encoding
        """
        return self.__INT_TO_HEPTAD[int(to_heptad)]

    @lru_cache(maxsize=4)
    def class_weights(self, normalise: bool = True, deleted: bool = False) -> np.ndarray:
        """
        Compute heptad position label weights according to their inverse frequency.

        :param normalise: normalise heptad position label weights to the sum of 1
        :param deleted: compute deleted heptad position label weights
        :return: computed heptad position label weights
        """
        with Pool() as worker_pool:
            counts = worker_pool.map(func=self._count_heptad_frequencies, iterable=self.y)
        counts = sum(counts, Counter())
        counts.pop(self.padding_sign_y)

        total = sum(_ for _ in counts.values())
        weights = {
            _: (((total - counts[_] if deleted else total) / counts[_]) if counts[_] > 0 else 0) for _ in counts}
        if normalise:
            total_weights = sum(_ for _ in weights.values())
            weights = {_: weights[_] / total_weights for _ in weights}

        return np.asarray([weights[k] for k in sorted(k for k in weights)], dtype=np.float32)

    @property
    def sequence_indices(self) -> List[int]:
        return self.__sequence_indices

    @sequence_indices.setter
    def sequence_indices(self, new_sequence_indices: List[int]) -> None:
        if new_sequence_indices is None:
            self._reset_indices()
        else:
            self.__sequence_indices = new_sequence_indices

        type(self).x.fget.cache_clear()
        type(self).y.fget.cache_clear()
        type(self).meta.fget.cache_clear()
        type(self).lengths.fget.cache_clear()
        type(self).class_weights.cache_clear()

    @property
    def meta_dim(self) -> int:
        return self.__meta_dim

    @property
    def input_dim(self) -> int:
        return self.__input_dim

    @property
    def output_dim(self) -> int:
        return self.__output_dim

    @property
    def window_offset(self) -> int:
        return self.__window_offset

    @property
    def window_size(self) -> int:
        return self.__window_offset * 2 + 1

    @property
    def padding_sign_x(self) -> int:
        return 0

    @property
    def padding_sign_y(self) -> int:
        return self.__output_dim

    @property
    def padding_sign_m(self) -> int:
        return 0

    @property
    @lru_cache(maxsize=1)
    def x(self) -> np.ndarray:
        x = [np.load(os.path.join(
            self.__checkpoint_dir, str(self.__sequence_clusters[_]), r'{}_x.npy'.format(_)))
            for _ in self.__sequence_indices]

        return np.stack([np.pad(
            _.squeeze(), pad_width=(0, self.__max_length - _.shape[0]),
            mode=r'constant', constant_values=self.padding_sign_x) for _ in x])

    @property
    @lru_cache(maxsize=1)
    def y(self) -> np.ndarray:
        try:
            y = [np.load(os.path.join(
                self.__checkpoint_dir, str(self.__sequence_clusters[_]), r'{}_y.npy'.format(_)))
                for _ in self.__sequence_indices]

            return np.stack([np.pad(
                _.squeeze(), pad_width=(0, self.__max_length - _.shape[0]),
                mode=r'constant', constant_values=self.padding_sign_y) for _ in y])
        except (IOError, ValueError):
            return None

    @property
    @lru_cache(maxsize=1)
    def meta(self) -> np.ndarray:
        m = [np.load(os.path.join(
            self.__checkpoint_dir, str(self.__sequence_clusters[_]), r'{}_m.npy'.format(_)))
            for _ in self.__sequence_indices]

        return np.stack([np.pad(
            _.squeeze(), pad_width=(0, self.__max_length - _.shape[0]),
            mode=r'constant', constant_values=self.padding_sign_m) for _ in m])

    @property
    @lru_cache(maxsize=1)
    def lengths(self) -> np.ndarray:
        return np.stack([np.load(os.path.join(
            self.__checkpoint_dir, str(self.__sequence_clusters[_]), r'{}_l.npy'.format(_))).squeeze()
                         for _ in self.__sequence_indices])

    @property
    def max_length(self) -> np.int32:
        return np.int32(self.__max_length)

    @max_length.setter
    def max_length(self, new_max_length: np.int32) -> None:
        self.__max_length = new_max_length
