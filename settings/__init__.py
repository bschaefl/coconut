import copy
import json
import math
import operator
import os

from enum import Enum
from utilities.decorators import method_accepts, method_bounds


class PreProcessingSettings(object):
    """
    Class holding all pre-processing specific settings.
    """

    class ScoringFamily(Enum):
        """
        Enum representing all possible scoring matrices.
        """
        IDENTITY = (r'identity', r'identity.mat')
        PFASUM = (r'pfasum', r'PFASUM{}.mat')
        PFASUM_DECIMAL = (r'pfasum_decimal', r'PFASUM{}.mat')

    def __init__(self, settings: dict) -> None:
        """
        Initialise all pre-processing specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.window_offset = settings[r'window_offset']
        self.outlier_factor = settings[r'outlier_factor']
        self.scoring_family = settings[r'scoring_family']
        self.scoring_version = settings[r'scoring_version']
        self.scoring_normalise = settings[r'scoring_normalise']

    @property
    def window_offset(self) -> int:
        return self._window_offset

    @window_offset.setter
    @method_bounds((0, math.inf))
    @method_accepts(int)
    def window_offset(self, window_offset_new: int) -> None:
        self._window_offset = int(window_offset_new)

    @property
    def outlier_factor(self) -> float:
        return self._outlier_factor

    @outlier_factor.setter
    @method_bounds((-1, math.inf))
    @method_accepts(float)
    def outlier_factor(self, outlier_factor_new: float) -> None:
        self._outlier_factor = float(outlier_factor_new)

    @property
    def scoring_family(self) -> ScoringFamily:
        return self._scoring_family

    @scoring_family.setter
    @method_accepts(str)
    def scoring_family(self, scoring_family_new: str) -> None:
        self._scoring_family = getattr(self.ScoringFamily, scoring_family_new.upper())

    @property
    def scoring_version(self) -> int:
        return self._scoring_version

    @scoring_version.setter
    @method_bounds((11, 100), modes=((operator.ge, operator.le),))
    @method_accepts(int)
    def scoring_version(self, scoring_version_new: int) -> None:
        self._scoring_version = int(scoring_version_new)

    @property
    def scoring_normalise(self) -> bool:
        return self._scoring_normalise

    @scoring_normalise.setter
    @method_accepts(bool)
    def scoring_normalise(self, scoring_normalise_new: bool) -> None:
        self._scoring_normalise = bool(scoring_normalise_new)


class OptimiserSettings(object):
    """
    Class holding all optimiser specific settings.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all optimiser specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.learning_rate = settings[r'learning_rate']
        self.beta1 = settings[r'beta1']
        self.beta2 = settings[r'beta2']
        self.epsilon = settings[r'epsilon']
        self.weight_decay = settings[r'weight_decay']
        self.amsgrad = settings[r'amsgrad']

    @property
    def learning_rate(self) -> float:
        return self._learning_rate

    @learning_rate.setter
    @method_bounds((0, math.inf), modes=((operator.gt, operator.lt),))
    @method_accepts(float)
    def learning_rate(self, learning_rate_new: float) -> None:
        self._learning_rate = float(learning_rate_new)

    @property
    def beta1(self) -> float:
        return self._beta1

    @beta1.setter
    @method_bounds((0, math.inf))
    @method_accepts(float)
    def beta1(self, beta1_new: float) -> None:
        self._beta1 = float(beta1_new)

    @property
    def beta2(self) -> float:
        return self._beta2

    @beta2.setter
    @method_bounds((0, math.inf))
    @method_accepts(float)
    def beta2(self, beta2_new: float) -> None:
        self._beta2 = float(beta2_new)

    @property
    def epsilon(self) -> float:
        return self._epsilon

    @epsilon.setter
    @method_bounds((0, math.inf))
    @method_accepts(float)
    def epsilon(self, epsilon_new: float) -> None:
        self._epsilon = float(epsilon_new)

    @property
    def weight_decay(self) -> float:
        return self._weight_decay

    @weight_decay.setter
    @method_bounds((0, 1), modes=((operator.ge, operator.le),))
    @method_accepts(float)
    def weight_decay(self, weight_decay_new: float) -> None:
        self._weight_decay = float(weight_decay_new)

    @property
    def amsgrad(self) -> bool:
        return self._amsgrad

    @amsgrad.setter
    @method_accepts(bool)
    def amsgrad(self, amsgrad_new: bool) -> None:
        self._amsgrad = bool(amsgrad_new)


class ArchitectureSettings(object):
    """
    Class holding all architecture specific settings.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all architecture specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.input_dropout = settings[r'input_dropout']
        self.extract_size = settings[r'extract_size']
        self.extract_layers = settings[r'extract_layers']
        self.extract_dropout = settings[r'extract_dropout']
        self.scan_size = settings[r'scan_size']
        self.scan_layers = settings[r'scan_layers']
        self.scan_dropout = settings[r'scan_dropout']
        self.project_dropout = settings[r'project_dropout']

    @property
    def input_dropout(self) -> float:
        return self._input_dropout

    @input_dropout.setter
    @method_bounds((0, 1), modes=((operator.ge, operator.le),))
    @method_accepts(float)
    def input_dropout(self, input_dropout_new: float) -> None:
        self._input_dropout = float(input_dropout_new)

    @property
    def extract_size(self) -> int:
        return self._extract_size

    @extract_size.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def extract_size(self, extract_size_new: int) -> None:
        self._extract_size = int(extract_size_new)

    @property
    def extract_layers(self) -> int:
        return self._extract_layers

    @extract_layers.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def extract_layers(self, extract_layers_new: int) -> None:
        self._extract_layers = int(extract_layers_new)

    @property
    def extract_dropout(self) -> float:
        return self._extract_dropout

    @extract_dropout.setter
    @method_bounds((0, 1), modes=((operator.ge, operator.le),))
    @method_accepts(float)
    def extract_dropout(self, extract_dropout_new: float) -> None:
        self._extract_dropout = float(extract_dropout_new)

    @property
    def scan_size(self) -> int:
        return self._scan_size

    @scan_size.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def scan_size(self, scan_size_new: int) -> None:
        self._scan_size = int(scan_size_new)

    @property
    def scan_layers(self) -> int:
        return self._scan_layers

    @scan_layers.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def scan_layers(self, scan_layers_new: int) -> None:
        self._scan_layers = int(scan_layers_new)

    @property
    def scan_dropout(self) -> float:
        return self._scan_dropout

    @scan_dropout.setter
    @method_bounds((0, 1), modes=((operator.ge, operator.le),))
    @method_accepts(float)
    def scan_dropout(self, scan_dropout_new: float) -> None:
        self._scan_dropout = float(scan_dropout_new)

    @property
    def project_dropout(self) -> float:
        return self._project_dropout

    @project_dropout.setter
    @method_bounds((0, 1), modes=((operator.ge, operator.le),))
    @method_accepts(float)
    def project_dropout(self, project_dropout_new: float) -> None:
        self._project_dropout = float(project_dropout_new)


class RunSettings(object):
    """
    Class holding all run specific settings.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all run specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.seed = settings[r'seed']
        self.cuda = settings[r'cuda']
        self.epochs = settings[r'epochs']
        self.evaluation_interval = settings[r'evaluation_interval']
        self.batch_size = settings[r'batch_size']
        self.shuffle = settings[r'shuffle']
        self.weight_classes = settings[r'weight_classes']
        self.weight_classes_normalised = settings[r'weight_classes_normalised']
        self.weight_classes_deleted = settings[r'weight_classes_deleted']

    @property
    def seed(self) -> int:
        return self._seed

    @seed.setter
    @method_bounds((-math.inf, math.inf), modes=((operator.gt, operator.lt),))
    @method_accepts(int)
    def seed(self, seed_new: int) -> None:
        self._seed = int(seed_new)

    @property
    def cuda(self) -> bool:
        return self._cuda

    @cuda.setter
    @method_accepts(bool)
    def cuda(self, cuda_new: bool) -> None:
        self._cuda = bool(cuda_new)

    @property
    def epochs(self) -> int:
        return self._epochs

    @epochs.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def epochs(self, epochs_new: int) -> None:
        self._epochs = int(epochs_new)

    @property
    def evaluation_interval(self) -> int:
        return self._evaluation_interval

    @evaluation_interval.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def evaluation_interval(self, evaluation_interval_new: int) -> None:
        self._evaluation_interval = int(evaluation_interval_new)

    @property
    def batch_size(self) -> int:
        return self._batch_size

    @batch_size.setter
    @method_bounds((1, math.inf))
    @method_accepts(int)
    def batch_size(self, batch_size_new: int) -> None:
        self._batch_size = int(batch_size_new)

    @property
    def shuffle(self) -> bool:
        return self._shuffle

    @shuffle.setter
    @method_accepts(bool)
    def shuffle(self, shuffle_new: bool) -> None:
        self._shuffle = bool(shuffle_new)

    @property
    def weight_classes(self) -> bool:
        return self._weight_classes

    @weight_classes.setter
    @method_accepts(bool)
    def weight_classes(self, weight_classes_new: bool) -> None:
        self._weight_classes = bool(weight_classes_new)

    @property
    def weight_classes_normalised(self) -> bool:
        return self._weight_classes_normalised

    @weight_classes_normalised.setter
    @method_accepts(bool)
    def weight_classes_normalised(self, weight_classes_normalised_new: bool) -> None:
        self._weight_classes_normalised = bool(weight_classes_normalised_new)

    @property
    def weight_classes_deleted(self) -> bool:
        return self._weight_classes_deleted

    @weight_classes_deleted.setter
    @method_accepts(bool)
    def weight_classes_deleted(self, weight_classes_deleted_new: bool) -> None:
        self._weight_classes_deleted = bool(weight_classes_deleted_new)


class DataSettings(object):
    """
    Class holding all data specific settings.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all data specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.training_file_path = settings[r'training_file_path']
        self.training_sample_separator = settings[r'training_sample_separator']
        self.training_distinct_samples = settings[r'training_distinct_samples']
        self.evaluation_file_path = settings[r'evaluation_file_path']
        self.evaluation_sample_separator = settings[r'evaluation_sample_separator']
        self.evaluation_distinct_samples = settings[r'evaluation_distinct_samples']

    @property
    def training_file_path(self) -> str:
        return self._training_file_path

    @training_file_path.setter
    @method_accepts(str)
    def training_file_path(self, training_file_path_new: str) -> None:
        self._training_file_path = str(training_file_path_new)

    @property
    def training_sample_separator(self) -> str:
        return self._training_sample_separator

    @training_sample_separator.setter
    @method_accepts(str)
    def training_sample_separator(self, training_sample_separator_new: str) -> None:
        self._training_sample_separator = str(training_sample_separator_new)

    @property
    def training_distinct_samples(self) -> bool:
        return self._training_distinct_samples

    @training_distinct_samples.setter
    @method_accepts(bool)
    def training_distinct_samples(self, training_distinct_samples_new: bool) -> None:
        self._training_distinct_samples = bool(training_distinct_samples_new)

    @property
    def evaluation_file_path(self) -> str:
        return self._evaluation_file_path

    @evaluation_file_path.setter
    @method_accepts(str)
    def evaluation_file_path(self, evaluation_file_path_new: str) -> None:
        self._evaluation_file_path = str(evaluation_file_path_new)

    @property
    def evaluation_sample_separator(self) -> str:
        return self._evaluation_sample_separator

    @evaluation_sample_separator.setter
    @method_accepts(str)
    def evaluation_sample_separator(self, evaluation_sample_separator_new: str) -> None:
        self._evaluation_sample_separator = str(evaluation_sample_separator_new)

    @property
    def evaluation_distinct_samples(self) -> bool:
        return self._evaluation_distinct_samples

    @evaluation_distinct_samples.setter
    @method_accepts(bool)
    def evaluation_distinct_samples(self, evaluation_distinct_samples_new: bool) -> None:
        self._evaluation_distinct_samples = bool(evaluation_distinct_samples_new)


class LogSettings(object):
    """
    Class holding all log specific settings.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all log specific settings.

        :param settings: settings dictionary to initialise from
        """
        self.checkpoint_directory = settings[r'checkpoint_directory']
        self.summary_directory = settings[r'summary_directory']
        self.overwrite = settings[r'overwrite']

    @property
    def checkpoint_directory(self) -> str:
        return self._checkpoint_directory

    @checkpoint_directory.setter
    @method_accepts(str)
    def checkpoint_directory(self, checkpoint_directory_new: str) -> None:
        self._checkpoint_directory = str(checkpoint_directory_new)

    @property
    def summary_directory(self) -> str:
        return self._summary_directory

    @summary_directory.setter
    @method_accepts(str)
    def summary_directory(self, summary_directory_new: str) -> None:
        self._summary_directory = str(summary_directory_new)

    @property
    def overwrite(self) -> bool:
        return self._overwrite

    @overwrite.setter
    @method_accepts(bool)
    def overwrite(self, overwrite_new: bool) -> None:
        self._overwrite = bool(overwrite_new)


class Settings(object):
    """
    Class holding all settings for reCOIL.
    """

    def __init__(self, settings: dict) -> None:
        """
        Initialise all settings.

        :param settings: settings dictionary to initialise from
        """
        defaults = os.path.join(os.path.dirname(os.path.realpath(__file__)), r'defaults.json')
        with open(file=defaults, mode=r'r') as defaults_json:
            defaults = json.load(fp=defaults_json)

        def merge_settings(_defaults: dict, _custom: dict) -> dict:
            _current = copy.deepcopy(_defaults)
            for settings_type, settings_value in _custom.items():
                if settings_type in _defaults:
                    _current[settings_type].update(settings_value)
                else:
                    _current[settings_type] = settings_value

            return _current

        settings = merge_settings(_defaults=defaults, _custom=settings)
        self.pre_processing = settings[r'pre_processing']
        self.optimiser = settings[r'optimiser']
        self.architecture = settings[r'architecture']
        self.run = settings[r'run']
        self.data = settings[r'data']
        self.log = settings[r'log']

    @classmethod
    def from_json_file(cls, file: str) -> r'Settings':
        with open(file=file, mode=r'r') as custom:
            return cls(json.load(fp=custom))

    @property
    def pre_processing(self) -> PreProcessingSettings:
        return self._pre_processing

    @pre_processing.setter
    @method_accepts(PreProcessingSettings)
    def pre_processing(self, pre_processing_new: dict) -> None:
        self._pre_processing = PreProcessingSettings(settings=pre_processing_new)

    @property
    def optimiser(self) -> OptimiserSettings:
        return self._optimiser

    @optimiser.setter
    @method_accepts(OptimiserSettings)
    def optimiser(self, optimiser_new: dict) -> None:
        self._optimiser = OptimiserSettings(settings=optimiser_new)

    @property
    def architecture(self) -> ArchitectureSettings:
        return self._architecture

    @architecture.setter
    @method_accepts(ArchitectureSettings)
    def architecture(self, architecture_new: dict) -> None:
        self._architecture = ArchitectureSettings(settings=architecture_new)

    @property
    def run(self) -> RunSettings:
        return self._run

    @run.setter
    @method_accepts(RunSettings)
    def run(self, run_new: dict) -> None:
        self._run = RunSettings(settings=run_new)

    @property
    def data(self) -> DataSettings:
        return self._data

    @data.setter
    @method_accepts(DataSettings)
    def data(self, data_new: dict) -> None:
        self._data = DataSettings(settings=data_new)

    @property
    def log(self) -> LogSettings:
        return self._log

    @log.setter
    @method_accepts(LogSettings)
    def log(self, log_new: dict) -> None:
        self._log = LogSettings(settings=log_new)
