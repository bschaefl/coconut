import os
import random

from Bio.Alphabet.IUPAC import IUPACProtein
from collections import namedtuple
from itertools import dropwhile
from settings import PreProcessingSettings
from typing import Union

DataSet = namedtuple(typename=r'DataSet', field_names=(r'x', r'y', r'c'), module=r'resources')


def combined_shuffle(data: Union[tuple, list]) -> tuple:
    """
    Shuffle data lists in a combined way.

    :param data: data to shuffle in a combined way
    :return: shuffled data as a tuple of lists
    """
    indices = list(range(len(data[0])))
    random.shuffle(indices)
    return tuple([data[_][i] for i in indices] for _ in range(len(data)))


def file_to_c_x_y(file_path: str, separator: str = r';', shuffle: bool = False) -> DataSet:
    """
    Split file into 'c', 'x' and 'y' subsets.

    :param file_path: input file from which to read
    :param separator: separator token of 'c', 'x' and 'y'
    :param shuffle: shuffle data
    :return: cluster id 'c', features 'x' and labels 'y' of data set
    """
    data = [[], [], []]
    with open(file_path, mode=r'r') as input_file:
        for line in input_file:
            if not line.startswith(r'#'):
                c, x, y = line.split(sep=separator, maxsplit=2)
                data[0].append(int(c.strip()))
                data[1].append(x.strip().upper())
                data[2].append(y.strip().lower())

    if (len(data[0]) != len(data[1])) or (len(data[0]) != len(data[2])):
        raise AttributeError(r'Provided data set file is invalid!')

    if shuffle:
        data = combined_shuffle(data=data)

    return DataSet(x=data[1], y=data[2], c=data[0])


def get_scoring_matrix(version: int, family: PreProcessingSettings.ScoringFamily, normalise: bool = False) -> dict:
    """
    Read and parse scoring matrix.

    :param version: version of the scoring matrix to read and parse
    :param family: family of the scoring matrix to read and parse
    :param normalise: normalise scoring matrix to be in the interval [-1;1]
    :return: parsed scoring matrix
    """
    if not isinstance(family, PreProcessingSettings.ScoringFamily):
        raise ValueError(r'Invalid scoring matrix family specified!')

    current_dir = os.path.dirname(os.path.realpath(__file__))
    matrix_version = family.value[1].format(version)
    matrix_path = os.path.join(current_dir, r'scoring_matrices', family.value[0], matrix_version)
    if not os.path.isfile(matrix_path):
        raise ValueError(r'Invalid scoring matrix version specified!')

    with open(matrix_path, mode=r'r') as scoring:
        lines = dropwhile(lambda _: _.startswith(r'#'), scoring)
        amino_acids = next(lines).split()
        final_matrix = {_: None for _ in amino_acids}

        for line in lines:
            current_scores = [(_ if i == 0 else float(_)) for i, _ in enumerate(line.split())]
            final_matrix[current_scores[0]] = {amino_acids[i]: _ for i, _ in enumerate(current_scores[1:])}

        # Remove ambiguous amino acids.
        for amino_acid in list(final_matrix):
            final_matrix[amino_acid] = {k: v for k, v in final_matrix[amino_acid].items() if k in IUPACProtein.letters}
            if amino_acid not in IUPACProtein.letters:
                final_matrix.pop(amino_acid)

        if normalise:
            min_value = min([min([_ for _ in k.values()]) for k in final_matrix.values()])
            max_value = max([max([_ for _ in k.values()]) for k in final_matrix.values()])
            for amino_acid in final_matrix:
                final_matrix[amino_acid] = {
                    k: 2 * (v - min_value) / (max_value - min_value) - 1 for k, v in final_matrix[amino_acid].items()}

    return final_matrix
