# CoCoNuT – An LSTM-based approach for coiled-coil domain prediction

Analysing proteins, especially ones with an yet unknown functionality or purpose, is a major task in structural biological and medical research. The corresponding three-dimensional structure is hereby of special interest, as it poses as a driving factor in determining a protein’s role. Methods aiding the process of gathering said information are thus in great demand. Although existing technologies like circular dichroism spectroscopy and nuclear magnetic resonance spectroscopy are already capable of analysing proteins regarding their structural properties – particularly α-helices and β-sheets – their application is rather expensive and time consuming. Hence, this thesis implements and elaborates an approach by utilising current state of the art techniques in Deep Learning, with a special emphasis on the Long Short-Term Memory architecture introduced by Hochreiter and Schmidhuber. The primary target of the developed system is the detection of regions in which two or more α-helices are wound around each other – so called coiled-coil domains – as they play a central role in diverse processes like cell invasion and regulation of gene expressions.

# Requirements

The software was developed and tested on the following 64-bit operating systems:

- Fedora 26 (Workstation Edition)
- MacOS 10.13.6 (High Sierra)

Nonetheless, it <i>should</i> run on any operating system, as long as all dependencies and requirements are met. As the development environment, [Python](https://www.python.org) 3.6.5 in combination with the following packages was deployed:

<b>For computational purposes:</b>

- [biopython](https://biopython.org) 1.71
- [scikit-learn](http://scikit-learn.org/stable/) 0.19.1
- [scipy](https://www.scipy.org) 1.1.0
- [torch](https://pytorch.org) 0.4.0

<b>For logging purposes:</b>

- [tensorboardX](https://github.com/lanpa/tensorboardX) 1.2
- [tqdm](https://github.com/tqdm/tqdm) 4.23.2

CoCoNuT has CUDA-support built in (via the [PyTorch](https://pytorch.org) project). Please refer to [NVIDIAs](https://developer.nvidia.com/cuda-zone) documentation on how to install and get CUDA running. Regardless of the absence or presence of a CUDA-enabled device, CoCoNuT can be configured to be executed as CPU-only.

# Usage

CoCoNuT has two modes of operation in total:

- `train` for training of a new model
- `predict` for predicting heptad repeat labels using an already trained model

A short documentation is included, and can be accessed by specifying the `-h` flag.

```bash
$ ./main.py -h
```

```bash
$ ./main.py MODE -h
```

The first operational mode is discussed in its own section. The second mode uses an already trained model (created by CoCoNuT) via the `-c` parameter, and applies it onto an amino acid string specified with `-a`.

```bash
$ ./main.py predict -c MODEL.pt -a AMINOACIDS
```

CoCoNuT is shipped with the final and best performing model of the accompanying thesis, located in [resources/models/](resources/models/). Exemplarily, an arbitrary amino acid sequence can be analysed using said model in the following way:

```bash
$ ./main.py predict -c resources/models/final.pt -a GSHMEEDPCECKSIVKFQTKVEELINTLQQKLEAVAKRIEALENKII
```

The resulting output (one heptad repeat label per initially specified amino acid) is displayed on its own line:

```
----------abcdefgabcdefgabcdefgabcdefgabcdefga-
```

No modes of operation supporting e.g. bunch analysis are currently available.

# Training of a model

To train an own model using CoCoNuT, the following command has to be used:

```bash
$ ./main.py train -s SETTINGS.json
```

All information needed to train the model (e.g. hyperparameters, data and logging directories) has to be specified via a settings file in the `json` format. The defaults, which act as a fallback in case of omitting a specific setting, can be seen in [settings/defaults.json](settings/defaults.json):

```json
{
  "pre_processing": {
    "window_offset": 4,
    "outlier_factor": -1,
    "scoring_family": "PFASUM_DECIMAL",
    "scoring_version": 60,
    "scoring_normalise": true
  },
  "optimiser": {
    "learning_rate": 1e-3,
    "beta1": 9e-1,
    "beta2": 999e-3,
    "epsilon": 1e-8,
    "weight_decay": 0,
    "amsgrad": false
  },
  "architecture": {
    "input_dropout": 0,
    "extract_size": 128,
    "extract_layers": 1,
    "extract_dropout": 0,
    "scan_size": 512,
    "scan_layers": 1,
    "scan_dropout": 0,
    "project_dropout": 0.50
  },
  "run": {
    "seed": 42,
    "cuda": true,
    "epochs": 100,
    "evaluation_interval": 1,
    "batch_size": 4,
    "shuffle": true,
    "weight_classes": true,
    "weight_classes_normalised": true,
    "weight_classes_deleted": false
  },
  "data": {
    "training_file_path": "resources/data/train_all.csv",
    "training_sample_separator": ";",
    "training_distinct_samples": true,
    "evaluation_file_path": "resources/data/eval_all.csv",
    "evaluation_sample_separator": ";",
    "evaluation_distinct_samples": true
  },
  "log": {
    "checkpoint_directory": "log/checkpoints/defaults/",
    "summary_directory": "log/summaries/defaults/",
    "overwrite": false
  }
}
```

The single settings should be self-explanatory, especially in combination with the accompanying thesis. If not, the associated [source file](settings/__init__.py) may deliver a more useful insight.

# References (excerpt)

- Billeter, M., Wagner, G., and Wüthrich, K. Solution NMR structure determination of proteins revisited. Journal of biomolecular NMR 42, 3 (2008), 155–158.

- Greenfield, N. J. Using circular dichroism spectra to estimate protein secondary structure. Nature protocols 1, 6 (2006), 2876.

- Hochreiter, S., and Schmidhuber, J. Long short-term memory. Neural computation 9, 8 (1997), 1735–1780.

- Gers, F. A., Schmidhuber, J., and Cummins, F. Learning to forget: Continual prediction with LSTM. Tech. rep., 1999.